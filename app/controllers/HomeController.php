<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
		return View::make('hello');
	}

	public function index()
	{
		$username = Auth::user()->username;
		return View::make('home')->with('username',$username);
	}

	public function login()
	{
		$title = 'Login Page';
		return View::make('login')->with('title',$title);
	}

	public function saveInfo()
	{
		$rules = [
			'email'=>'email|required|unique:users',
			'password'=>'required'
		];
		$validator = Validator::make(Input::all(),$rules);
		if (!$validator->fails()) 
		{
			$newUser = new User;
			$newUser->username = Input::get('username');
			$newUser->email = Input::get('email');
			$newUser->password = Hash::make(Input::get('password'));
			$newUser->save();
			return Redirect::to('/')->with('message','your account has been created successfully.Please login !<br><br>');
		}
		else
		{
			Redirect::to('/')->withErrors($validator);
		}

	}

	public function authentication()
	{
		
		$user = array(
			'email'=> Input::get('email'),
			'password'=> Input::get('password')
		);
		// return $user['password'] ;
		if(Auth::attempt($user))
		{
			return Redirect::to('home');
		}
		else
		{
			return Redirect::to('login')->with('message','Email/Password combination is incorrect. Please try again <br><br>');
		}
	}

	public function signUp()
	{
		$title = 'Sign Up';
		return View::make('signup')->with('title',$title);
	}

	public function logout()
	{
		if (Auth::check()) 
		{
			Auth::logout();
			return Redirect::route('login')->with('message','You are successfully logged out. Thank you !<br><br>');
		}
		else
		{
			return Redirect::route('home');
		}
	}

}
