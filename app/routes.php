<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/','HomeController@login');
Route::get('login',['as'=>'login','uses'=>'HomeController@login']);
Route::post('login',['before'=>'CSRF','as'=>'login','uses'=>'HomeController@authentication']);
Route::get('logout',['as'=>'logout','uses'=>'HomeController@logout']);
Route::get('register',['as'=>'register','uses'=>'HomeController@signUp']);
Route::post('register',['before'=>'csrf','as'=>'register','uses'=>'HomeController@saveInfo']);
Route::group(['before' => 'auth'], function()
{
	Route::get('home',['as'=>'home','uses'=>'HomeController@index']);
});
